# Base de datos Alumnos

Imagina una simple base de datos donde se almacena información de las calificaciones de un grupo de alumnos. Diseña cómo pudiera lucir esta base de datos y genera una consulta que obtenga los 10 mejores alumnos ordenados por su promedio.