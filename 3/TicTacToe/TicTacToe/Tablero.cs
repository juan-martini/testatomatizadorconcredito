﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Tablero : Form
    {
        bool turno = true;
        int Contador = 0;

        Jugador JugadorUno = new Jugador();
        Jugador JugadorDos = new Jugador();
        
        public Tablero()
        {
            InitializeComponent();
            txtJ1.Select();
        }

        private void btnEmpezar_Click(object sender, EventArgs e)
        {
            if (txtJ1.Text.Trim().Length > 0 && txtJ2.Text.Trim().Length > 0)
            {

                JugadorUno.Nombre = txtJ1.Text.Trim();
                JugadorDos.Nombre = txtJ2.Text.Trim();

                prepararIniciar();

            }
            else
            {
                MessageBox.Show("Favor de capturar el nombre de los jugadores");
            }
        }

        private void prepararIniciar()
        {
            lblJ1.Visible = false;
            lblJ2.Visible = false;
            lblIngre.Visible = false;
            txtJ1.Visible = false;
            txtJ1.Enabled = false;
            txtJ2.Visible = false;
            txtJ2.Enabled = false;

            btnEmpezar.Visible = false;

            p1.Visible = true;
            p2.Visible = true;
            p3.Visible = true;
            p4.Visible = true;
            p5.Visible = true;
            p6.Visible = true;
            p7.Visible = true;
            p8.Visible = true;
            p9.Visible = true;
        }

        private void button_click(object sender, EventArgs e)
        {
            
            try {
                Button b = (Button)sender;

                if(turno) {
                    b.Text = "X";
                } else {
                    b.Text = "O";
                }

                turno = !turno;
                Contador++;
                b.Enabled = false;

                validarGanador();

            } catch {

            }
        }

        private void reiniciarjuego()
        {
            foreach(Control b in Controls.OfType<Button>()) {
                b.Enabled = true;
                b.Text = "";
            }

            Contador = 0;
            turno = true;
        }

        public void validarGanador()
        {
            bool gano = false;

            // Revision horizontal
            if((p1.Text != "") && (p1.Text == p2.Text) && (p2.Text == p3.Text))
                gano = true;
            else if ((p4.Text != "") && (p4.Text == p5.Text) && (p5.Text == p6.Text))
                gano = true;
            else if ((p7.Text != "") && (p7.Text == p8.Text) && (p8.Text == p9.Text))
                gano = true;

            // Revision vertical
            else if ((p1.Text != "") && (p1.Text == p4.Text) && (p4.Text == p7.Text))
                gano = true;
            else if ((p2.Text != "") && (p2.Text == p5.Text) && (p5.Text == p8.Text))
                gano = true;
            else if ((p3.Text != "") && (p3.Text == p6.Text) && (p6.Text == p9.Text))
                gano = true;

            // Revision diagonal
            else if ((p1.Text != "") && (p1.Text == p5.Text) && (p5.Text == p9.Text))
                gano = true;
            else if ((p3.Text != "") && (p3.Text == p5.Text) && (p5.Text == p7.Text))
                gano = true;

            if(gano) {
                string ganador = "";

                if (turno) {
                    ganador = JugadorDos.Nombre;
                }
                else {
                    ganador = JugadorUno.Nombre;
                }

                MessageBox.Show("El ganador es: " + ganador);

                reiniciarjuego();
            } else if(Contador == 9) {
                MessageBox.Show("No hay ganador");

                reiniciarjuego();
            }
        }
    }
}
