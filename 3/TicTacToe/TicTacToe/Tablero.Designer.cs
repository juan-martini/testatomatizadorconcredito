﻿namespace TicTacToe
{
    partial class Tablero
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.p1 = new System.Windows.Forms.Button();
            this.p3 = new System.Windows.Forms.Button();
            this.p2 = new System.Windows.Forms.Button();
            this.p4 = new System.Windows.Forms.Button();
            this.p5 = new System.Windows.Forms.Button();
            this.p6 = new System.Windows.Forms.Button();
            this.p7 = new System.Windows.Forms.Button();
            this.p8 = new System.Windows.Forms.Button();
            this.p9 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblJ1 = new System.Windows.Forms.Label();
            this.lblJ2 = new System.Windows.Forms.Label();
            this.txtJ1 = new System.Windows.Forms.TextBox();
            this.txtJ2 = new System.Windows.Forms.TextBox();
            this.lblIngre = new System.Windows.Forms.Label();
            this.btnEmpezar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // p1
            // 
            this.p1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p1.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p1.Location = new System.Drawing.Point(175, 109);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(125, 90);
            this.p1.TabIndex = 0;
            this.p1.UseVisualStyleBackColor = true;
            this.p1.Visible = false;
            this.p1.Click += new System.EventHandler(this.button_click);
            // 
            // p3
            // 
            this.p3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p3.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p3.Location = new System.Drawing.Point(437, 109);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(125, 90);
            this.p3.TabIndex = 0;
            this.p3.UseVisualStyleBackColor = true;
            this.p3.Visible = false;
            this.p3.Click += new System.EventHandler(this.button_click);
            // 
            // p2
            // 
            this.p2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p2.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p2.Location = new System.Drawing.Point(306, 109);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(125, 90);
            this.p2.TabIndex = 0;
            this.p2.UseVisualStyleBackColor = true;
            this.p2.Visible = false;
            this.p2.Click += new System.EventHandler(this.button_click);
            // 
            // p4
            // 
            this.p4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p4.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p4.Location = new System.Drawing.Point(175, 205);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(125, 90);
            this.p4.TabIndex = 0;
            this.p4.UseVisualStyleBackColor = true;
            this.p4.Visible = false;
            this.p4.Click += new System.EventHandler(this.button_click);
            // 
            // p5
            // 
            this.p5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p5.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p5.Location = new System.Drawing.Point(306, 205);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(125, 90);
            this.p5.TabIndex = 0;
            this.p5.UseVisualStyleBackColor = true;
            this.p5.Visible = false;
            this.p5.Click += new System.EventHandler(this.button_click);
            // 
            // p6
            // 
            this.p6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p6.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p6.Location = new System.Drawing.Point(437, 205);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(125, 90);
            this.p6.TabIndex = 0;
            this.p6.UseVisualStyleBackColor = true;
            this.p6.Visible = false;
            this.p6.Click += new System.EventHandler(this.button_click);
            // 
            // p7
            // 
            this.p7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p7.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p7.Location = new System.Drawing.Point(175, 301);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(125, 90);
            this.p7.TabIndex = 0;
            this.p7.UseVisualStyleBackColor = true;
            this.p7.Visible = false;
            this.p7.Click += new System.EventHandler(this.button_click);
            // 
            // p8
            // 
            this.p8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p8.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p8.Location = new System.Drawing.Point(306, 301);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(125, 90);
            this.p8.TabIndex = 0;
            this.p8.UseVisualStyleBackColor = true;
            this.p8.Visible = false;
            this.p8.Click += new System.EventHandler(this.button_click);
            // 
            // p9
            // 
            this.p9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.p9.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.p9.Location = new System.Drawing.Point(437, 301);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(125, 90);
            this.p9.TabIndex = 0;
            this.p9.UseVisualStyleBackColor = true;
            this.p9.Visible = false;
            this.p9.Click += new System.EventHandler(this.button_click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(292, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 77);
            this.label1.TabIndex = 1;
            this.label1.Text = "Gato";
            // 
            // lblJ1
            // 
            this.lblJ1.AutoSize = true;
            this.lblJ1.Location = new System.Drawing.Point(258, 197);
            this.lblJ1.Name = "lblJ1";
            this.lblJ1.Size = new System.Drawing.Size(61, 15);
            this.lblJ1.TabIndex = 2;
            this.lblJ1.Text = "Jugador 1:";
            // 
            // lblJ2
            // 
            this.lblJ2.AutoSize = true;
            this.lblJ2.Location = new System.Drawing.Point(258, 242);
            this.lblJ2.Name = "lblJ2";
            this.lblJ2.Size = new System.Drawing.Size(61, 15);
            this.lblJ2.TabIndex = 2;
            this.lblJ2.Text = "Jugador 2:";
            // 
            // txtJ2
            // 
            this.txtJ2.Location = new System.Drawing.Point(325, 242);
            this.txtJ2.Name = "txtJ2";
            this.txtJ2.Size = new System.Drawing.Size(187, 23);
            this.txtJ2.TabIndex = 3;
            // 
            // txtJ1
            // 
            this.txtJ1.Location = new System.Drawing.Point(325, 194);
            this.txtJ1.Name = "txtJ1";
            this.txtJ1.Size = new System.Drawing.Size(187, 23);
            this.txtJ1.TabIndex = 3;
            // 
            // lblIngre
            // 
            this.lblIngre.AutoSize = true;
            this.lblIngre.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblIngre.Location = new System.Drawing.Point(258, 109);
            this.lblIngre.Name = "lblIngre";
            this.lblIngre.Size = new System.Drawing.Size(258, 20);
            this.lblIngre.TabIndex = 4;
            this.lblIngre.Text = "Ingrese el nombre de los jugadores:";
            // 
            // btnEmpezar
            // 
            this.btnEmpezar.Location = new System.Drawing.Point(365, 333);
            this.btnEmpezar.Name = "btnEmpezar";
            this.btnEmpezar.Size = new System.Drawing.Size(75, 23);
            this.btnEmpezar.TabIndex = 5;
            this.btnEmpezar.Text = "Empezar";
            this.btnEmpezar.UseVisualStyleBackColor = true;
            this.btnEmpezar.Click += new System.EventHandler(this.btnEmpezar_Click);
            // 
            // Tablero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnEmpezar);
            this.Controls.Add(this.lblIngre);
            this.Controls.Add(this.txtJ2);
            this.Controls.Add(this.txtJ1);
            this.Controls.Add(this.lblJ2);
            this.Controls.Add(this.lblJ1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.p9);
            this.Controls.Add(this.p8);
            this.Controls.Add(this.p7);
            this.Controls.Add(this.p6);
            this.Controls.Add(this.p5);
            this.Controls.Add(this.p4);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p1);
            this.Name = "Tablero";
            this.Text = "Juego del gato";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button p1;
        private System.Windows.Forms.Button p3;
        private System.Windows.Forms.Button p2;
        private System.Windows.Forms.Button p4;
        private System.Windows.Forms.Button p5;
        private System.Windows.Forms.Button p6;
        private System.Windows.Forms.Button p7;
        private System.Windows.Forms.Button p8;
        private System.Windows.Forms.Button p9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblJ1;
        private System.Windows.Forms.Label lblJ2;
        private System.Windows.Forms.TextBox txtJ1;
        private System.Windows.Forms.TextBox txtJ2;
        private System.Windows.Forms.Label lblIngre;
        private System.Windows.Forms.Button btnEmpezar;
    }
}

